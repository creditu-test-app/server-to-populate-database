# Server to populate firestore database

Small service to run script and populate database

## Run

Install node packages 
```bash
npm install
```
Run script

```bash
node data.js
```
